package com.nespresso.sofa.recruitement.tournament.warriors;

import com.nespresso.sofa.recruitement.tournament.warriors.defenses.DefenseFactory;
import com.nespresso.sofa.recruitement.tournament.warriors.weapons.WeaponFactory;

final class EquipmentBuilder {

	public static void build(Equipment equipment, String newEquipement) {
		if(WeaponFactory.hasEquipment(newEquipement)){
			equipment.saveWeapon(WeaponFactory.create(newEquipement));
		}
		if(DefenseFactory.hasEquipment(newEquipement)){
			equipment.saveDefense(DefenseFactory.create(newEquipement));
		}
	}
}

package com.nespresso.sofa.recruitement.tournament.warriors.weapons;

enum WeaponTypes {

	AXE("axe") {
		@Override
		WarriorWeapon make() {
			return new Axe();
		}
	},
	SWORD("sword") {
		@Override
		WarriorWeapon make() {
			return new Sword();
		}
	},
	GREATSWORD("greatsword") {
		@Override
		WarriorWeapon make() {
			return new GreatSword();
		}
	};
	
	private final String name;
	
	private WeaponTypes(String weaponName){
		name = weaponName;
	}
	
	static WeaponTypes fromStr(String weaponName){
		for (WeaponTypes weapon : WeaponTypes.values()) {
			if(weapon.name.equals(weaponName)){
				return weapon;
			}
		}
		throw new IllegalArgumentException("weaponName not Found");
	}
	
	abstract WarriorWeapon make();
}

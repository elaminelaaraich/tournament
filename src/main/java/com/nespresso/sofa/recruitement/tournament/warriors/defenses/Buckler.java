package com.nespresso.sofa.recruitement.tournament.warriors.defenses;

import com.nespresso.sofa.recruitement.tournament.warriors.weapons.BucklerDestroyer;
import com.nespresso.sofa.recruitement.tournament.warriors.weapons.WarriorWeapon;

class Buckler implements WarriorDefense {

	private static final int MAX_BLOWS_FROM_DESTROYING_WEAPON = 3;
	private static final int DISABLED_TURN = 2;
	

	private int turn = 1;
	private int receivedBlowsFromDestroyingWeapon;

	public int reduceDamage(WarriorWeapon usedWeapon, int hitPointsPower) {
		if (hitPointsPower>0 && bucklerStatus() == BucklerStatus.ACTIVE) {
			destroyBucklerIfAble(usedWeapon);
			turn++;
			return 0;
		}
		return hitPointsPower;
	}

	private void destroyBucklerIfAble(WarriorWeapon usedWeapon) {
		if(BucklerDestroyer.class.isAssignableFrom(usedWeapon.getClass())){
			receivedBlowsFromDestroyingWeapon++;
		}
	}

	private BucklerStatus bucklerStatus() {
		if (receivedBlowsFromDestroyingWeapon == MAX_BLOWS_FROM_DESTROYING_WEAPON) {
			return BucklerStatus.DISABLED;
		}
		if (turn == DISABLED_TURN) {
			turn = 1;
			return BucklerStatus.DISABLED;
		}
		return BucklerStatus.ACTIVE;
	}

	private enum BucklerStatus {
		DISABLED, ACTIVE
	}

}

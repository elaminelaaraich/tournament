package com.nespresso.sofa.recruitement.tournament.warriors;

import com.nespresso.sofa.recruitement.tournament.warriors.abilities.WarriorAbility;
import com.nespresso.sofa.recruitement.tournament.warriors.abilities.WarriorAbilityFactory;
import com.nespresso.sofa.recruitement.tournament.warriors.weapons.WarriorWeapon;

public abstract class Warrior {

	private static final String NO_ABILITY = "nullAbility";

	protected int currentHitPointStatus;
	protected Equipment equipment = new Equipment();
	protected WarriorAbility ability = WarriorAbilityFactory.create(NO_ABILITY);

	protected Warrior(final int initialHitPoint, String defaultWeapon) {
		currentHitPointStatus = initialHitPoint;
		equip(defaultWeapon);
	}

	public void engage(Warrior enemy) {
		fightToDeath(enemy);
	}

	private void fightToDeath(Warrior enemy) {
		hit(enemy);
		enemy.hit(this);
		if (warriorsAlive(enemy)) {
			fightToDeath(enemy);
		}
	}

	protected void hit(Warrior enemy) {
		if (currentHitPointStatus > 0) {
			equipment.causeDamage(enemy, ability);
		}
	}

	private boolean warriorsAlive(Warrior enemy) {
		return (currentHitPointStatus > 0 && enemy.currentHitPointStatus > 0);
	}

	public void receiveDamage(final int hitPointsDamage) {
		currentHitPointStatus = (currentHitPointStatus > hitPointsDamage ? currentHitPointStatus - hitPointsDamage : 0);
	}

	public int hitPoints() {
		return currentHitPointStatus;
	}

	public <T extends Warrior> T equip(String newEquipement) {
		EquipmentBuilder.build(equipment, newEquipement);
		return (T) this;
	}

	public int defend(WarriorWeapon usedWeapon, int hitPointsPower) {
		return equipment.defend(usedWeapon, hitPointsPower);
	}
}

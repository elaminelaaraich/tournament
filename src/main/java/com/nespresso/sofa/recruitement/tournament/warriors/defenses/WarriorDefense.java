package com.nespresso.sofa.recruitement.tournament.warriors.defenses;

import com.nespresso.sofa.recruitement.tournament.warriors.weapons.WarriorWeapon;

public interface WarriorDefense {

	int reduceDamage(WarriorWeapon usedWeapon, int hitPointsPower);
}

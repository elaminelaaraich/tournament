package com.nespresso.sofa.recruitement.tournament.warriors.weapons;

import com.nespresso.sofa.recruitement.tournament.warriors.Warrior;
import com.nespresso.sofa.recruitement.tournament.warriors.abilities.WarriorAbility;

class GreatSword extends Weapon {

	private static final int DISABLED_TURN = 3;
	private static final int HIT_POINT_DAMAGE_POWER = 12;

	private int turn = 1;

	GreatSword() {
		super(HIT_POINT_DAMAGE_POWER);
	}

	@Override
	public void causeDamage(Warrior warrior, WarriorAbility ability) {
		if (checkTurn()) {
			super.causeDamage(warrior,ability);
		}
	}

	private boolean checkTurn() {
		if (turn == DISABLED_TURN) {
			turn = 1;
			return false;
		}
		turn++;
		return true;
	}

}

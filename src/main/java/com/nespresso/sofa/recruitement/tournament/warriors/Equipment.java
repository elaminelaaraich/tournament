package com.nespresso.sofa.recruitement.tournament.warriors;

import java.util.ArrayList;
import java.util.List;

import com.nespresso.sofa.recruitement.tournament.warriors.abilities.WarriorAbility;
import com.nespresso.sofa.recruitement.tournament.warriors.defenses.AttackPenalty;
import com.nespresso.sofa.recruitement.tournament.warriors.defenses.WarriorDefense;
import com.nespresso.sofa.recruitement.tournament.warriors.weapons.WarriorWeapon;

class Equipment {

	private WarriorWeapon weapon;
	private List<WarriorDefense> defenses = new ArrayList<>();
	
	void causeDamage(Warrior enemy, WarriorAbility ability) {
		weapon.causeDamage(enemy,ability);
	}

	int defend(WarriorWeapon usedWeapon, int hitPointsPower) {
		int reducedDamage = hitPointsPower;
		for (WarriorDefense warriorDefense : defenses) {
			reducedDamage = warriorDefense.reduceDamage(usedWeapon, reducedDamage);
		}
		return reducedDamage;
	}

	void saveWeapon(WarriorWeapon newWeapon) {
		weapon = newWeapon;
	}
	
	void saveDefense(WarriorDefense newDefense) {
		defenses.add(newDefense);
		if(AttackPenalty.class.isAssignableFrom(newDefense.getClass())){
			AttackPenalty penalty = (AttackPenalty) newDefense;
			weapon.penalty(penalty.calculateAttackPenalty());
		}
	}
}

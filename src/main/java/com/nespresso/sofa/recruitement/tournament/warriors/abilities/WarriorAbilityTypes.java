package com.nespresso.sofa.recruitement.tournament.warriors.abilities;

enum WarriorAbilityTypes {

	VICIOUS("Vicious") {
		@Override
		WarriorAbility make() {
			return new ViciousAbility();
		}
	},
	BERSERK("Veteran") {
		@Override
		WarriorAbility make() {
			return new BerserkAbility();
		}
	},
	NULLABILITY("nullAbility") {
		@Override
		WarriorAbility make() {
			return new WarriorAbility();
		}
	};
	
	private final String name;
	
	private WarriorAbilityTypes(String abilityName){
		name = abilityName;
	}
	
	static WarriorAbilityTypes fromStr(String abilityName){
		for (WarriorAbilityTypes ability : WarriorAbilityTypes.values()) {
			if(ability.name.equals(abilityName)){
				return ability;
			}
		}
		throw new IllegalArgumentException("abilityName not Found");
	}
	
	abstract WarriorAbility make();
	
	@Override
	public String toString() {
		return name;
	}
}

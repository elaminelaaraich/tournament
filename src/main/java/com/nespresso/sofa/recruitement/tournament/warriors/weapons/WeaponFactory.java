package com.nespresso.sofa.recruitement.tournament.warriors.weapons;

public final class WeaponFactory {

	private WeaponFactory() {
	}

	public static WarriorWeapon create(String name) {
		WeaponTypes weapon = WeaponTypes.fromStr(name);
		return weapon.make();
	}

	public static boolean hasEquipment(String name) {
		try {
			WeaponTypes.fromStr(name);
			return true;
		} catch (Exception e) {
			return false;
		}
	}
}

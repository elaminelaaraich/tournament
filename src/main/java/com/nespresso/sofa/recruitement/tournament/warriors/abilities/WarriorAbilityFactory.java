package com.nespresso.sofa.recruitement.tournament.warriors.abilities;

public final class WarriorAbilityFactory {

	private WarriorAbilityFactory() {
	}

	public static WarriorAbility create(String abilityName) {
		WarriorAbilityTypes warriorAbilityType = WarriorAbilityTypes.fromStr(abilityName);
		return warriorAbilityType.make();
	}
}

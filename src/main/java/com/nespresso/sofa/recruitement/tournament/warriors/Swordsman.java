package com.nespresso.sofa.recruitement.tournament.warriors;

import com.nespresso.sofa.recruitement.tournament.warriors.abilities.WarriorAbilityFactory;

public class Swordsman extends Warrior {

	private static final int HIT_POINT = 100;
	private static final String DEFAULT_WEAPON = "sword";

	public Swordsman() {
		super(HIT_POINT, DEFAULT_WEAPON);
	}

	public Swordsman(String abilityName) {
		super(HIT_POINT, DEFAULT_WEAPON);
		ability = WarriorAbilityFactory.create(abilityName);
	}
}

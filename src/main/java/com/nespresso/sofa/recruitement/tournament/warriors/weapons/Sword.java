package com.nespresso.sofa.recruitement.tournament.warriors.weapons;

class Sword extends Weapon {

	private final static int HIT_POINT_DAMAGE_POWER = 5;

	public Sword() {
		super(HIT_POINT_DAMAGE_POWER);
	}

}

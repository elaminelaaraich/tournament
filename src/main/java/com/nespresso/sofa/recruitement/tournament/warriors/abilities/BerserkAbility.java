package com.nespresso.sofa.recruitement.tournament.warriors.abilities;

class BerserkAbility extends WarriorAbility {

	private static final int PERCENTAGE_ADDITIONAL_DAMAGE = 2;

	@Override
	public int useAbility(int hitPointDamage) {
		return hitPointDamage * PERCENTAGE_ADDITIONAL_DAMAGE;
	}

}

package com.nespresso.sofa.recruitement.tournament.warriors;

public class Viking extends Warrior {

	private static final int HIT_POINT = 120;
	private static final String DEFAULT_WEAPON = "axe";

	public Viking(){
		super(HIT_POINT, DEFAULT_WEAPON);
	}
}

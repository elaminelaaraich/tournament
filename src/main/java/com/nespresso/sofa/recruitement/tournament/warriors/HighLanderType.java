package com.nespresso.sofa.recruitement.tournament.warriors;

enum HighLanderType {

	NORMAL("nullAbility"),
	VETERAN("Veteran");
	
	private final String name;
	
	private HighLanderType(final String highLander){
		name = highLander;
	}
	
	@Override
	public String toString() {
		return name;
	}
}

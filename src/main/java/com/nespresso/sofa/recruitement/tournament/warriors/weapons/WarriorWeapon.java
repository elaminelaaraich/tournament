package com.nespresso.sofa.recruitement.tournament.warriors.weapons;

import com.nespresso.sofa.recruitement.tournament.warriors.Warrior;
import com.nespresso.sofa.recruitement.tournament.warriors.abilities.WarriorAbility;

public interface WarriorWeapon {

	void causeDamage(Warrior warrior, WarriorAbility ability);

	void penalty(int hitPointPenalty);
}

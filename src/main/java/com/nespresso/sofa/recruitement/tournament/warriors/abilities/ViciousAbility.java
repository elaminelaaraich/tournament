package com.nespresso.sofa.recruitement.tournament.warriors.abilities;

class ViciousAbility extends WarriorAbility {

	private static final int ADDITIONAL_DAMAGE = 20;
	private int countUsed;

	@Override
	public int useAbility(int hitPointDamage) {
		if(countUsed<2){
			countUsed++;
			return hitPointDamage + ADDITIONAL_DAMAGE;
		}
		return hitPointDamage;
	}
}

package com.nespresso.sofa.recruitement.tournament.warriors.defenses;

public interface AttackPenalty {

	int calculateAttackPenalty();
}

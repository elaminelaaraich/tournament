package com.nespresso.sofa.recruitement.tournament.warriors;

import com.nespresso.sofa.recruitement.tournament.warriors.abilities.WarriorAbilityFactory;

public class Highlander extends Warrior {

	private final static int HIT_POINT = 150;
	private static final String DEFAULT_WEAPON = "greatsword";
	private HighLanderType highlanderType;

	public Highlander() {
		super(HIT_POINT, DEFAULT_WEAPON);
		highlanderType = HighLanderType.NORMAL;
	}

	public Highlander(String abilityName) {
		super(HIT_POINT, DEFAULT_WEAPON);
		highlanderType = HighLanderType.VETERAN;
	}

	@Override
	public void receiveDamage(int hitPointsDamage) {
		super.receiveDamage(hitPointsDamage);
		if (HighLanderType.VETERAN.equals(highlanderType) && remainLess30PercentDamage()) {
			ability = WarriorAbilityFactory.create(highlanderType.toString());
		}
	}

	private boolean remainLess30PercentDamage() {
		float thirtyPercentOfInitialHitPoint = HIT_POINT - (HIT_POINT * 0.7f);
		if (currentHitPointStatus > thirtyPercentOfInitialHitPoint) {
			return false;
		}
		return true;
	}
}

package com.nespresso.sofa.recruitement.tournament.warriors.defenses;

enum DefenseTypes {

	BUCKLER("buckler") {
		@Override
		WarriorDefense make() {
			return new Buckler();
		}
	},
	ARMOR("armor") {
		@Override
		WarriorDefense make() {
			return new Armor();
		}
	};
	
	private String name;
	
	private DefenseTypes(String defenseName) {
		name = defenseName;
	}
	
	static DefenseTypes fromStr(String defenseName){
		for (DefenseTypes defense : DefenseTypes.values()) {
			if(defense.name.equals(defenseName)){
				return defense;
			}
		}
		throw new IllegalArgumentException("defenseName not Found");
	}
	
	abstract WarriorDefense make();
}

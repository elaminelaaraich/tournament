package com.nespresso.sofa.recruitement.tournament.warriors.weapons;

import com.nespresso.sofa.recruitement.tournament.warriors.Warrior;
import com.nespresso.sofa.recruitement.tournament.warriors.abilities.WarriorAbility;

abstract class Weapon implements WarriorWeapon {

	protected int deliveredHitPoint;

	Weapon(final int weaponDamagePower) {
		deliveredHitPoint = weaponDamagePower;
	}

	public void causeDamage(Warrior warrior, WarriorAbility ability) {
		int hitPointWithAbility = ability.useAbility(deliveredHitPoint);
		final int damageAfterDefense = warrior.defend(this, hitPointWithAbility);
		warrior.receiveDamage(damageAfterDefense);
	}

	public void penalty(int hitPointPenalty) {
		deliveredHitPoint = (deliveredHitPoint > hitPointPenalty ? deliveredHitPoint - hitPointPenalty : 0);
	}
}

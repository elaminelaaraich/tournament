package com.nespresso.sofa.recruitement.tournament.warriors.defenses;

import com.nespresso.sofa.recruitement.tournament.warriors.weapons.WarriorWeapon;

class Armor implements WarriorDefense, AttackPenalty {

	private static final int REDUCE_RECEIVED_DAMAGE = 3;
	private static final int REDUCE_DELIVERED_DAMAGE = 1;

	public int reduceDamage(WarriorWeapon usedWeapon, int hitPointsPower) {
		return (hitPointsPower > REDUCE_RECEIVED_DAMAGE ? hitPointsPower - REDUCE_RECEIVED_DAMAGE : 0);
	}

	public int calculateAttackPenalty() {
		return REDUCE_DELIVERED_DAMAGE;
	}

}

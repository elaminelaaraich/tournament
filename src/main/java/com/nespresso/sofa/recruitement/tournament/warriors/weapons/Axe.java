package com.nespresso.sofa.recruitement.tournament.warriors.weapons;

class Axe extends Weapon implements BucklerDestroyer {

	private final static int HIT_POINT_DAMAGE_POWER = 6;

	public Axe() {
		super(HIT_POINT_DAMAGE_POWER);
	}

}

package com.nespresso.sofa.recruitement.tournament.warriors.defenses;

public final class DefenseFactory {

	private DefenseFactory(){}

	public static WarriorDefense create(String name) {
		DefenseTypes defense = DefenseTypes.fromStr(name);
		return defense.make();
	}
	
	public static boolean hasEquipment(String name){
		try {
			DefenseTypes.fromStr(name);
			return true;
		} catch (Exception e) {
			return false;
		}
	}
}
